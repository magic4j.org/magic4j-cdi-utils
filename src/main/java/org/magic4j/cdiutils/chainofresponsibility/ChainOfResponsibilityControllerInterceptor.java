/*
 * Copyright 2016 Stephan Bauer <sb at stephanbauer.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.magic4j.cdiutils.chainofresponsibility;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.Set;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import org.magic4j.cdiutils.AnnotationUtil;

/**
 * The interceptor that detects and invokes all @ChainOfResponsibilityHandler
 * beans that have a method with the same custom CDI-Qualifier as the one that
 * must be present on the intercepted method. Invokes these Handlers until one
 * handler delivers a non-empty Optional-instance. If no handler delivers such a
 * non-empty Optional, an IllegalStateException is thrown.
 *
 * @author Stephan Bauer
 */
@ChainOfResponsibilityController
@Interceptor
public class ChainOfResponsibilityControllerInterceptor {

    @Inject
    private BeanManager beanManager;

    @Inject
    private AnnotationUtil annotationUtil;

    @AroundInvoke
    public Object detectAndCallChainHandlers(InvocationContext invocationContext) throws Exception {
        Method interceptedMethod = invocationContext.getMethod();
        assertMethodReturnsOptional(interceptedMethod);
        Set<Bean<?>> actualChainHandlers = detectChainHandlers(interceptedMethod);
        Optional<?> result = processChainHandlers(actualChainHandlers, interceptedMethod, invocationContext);
        return result;
    }

    /**
     *
     * @param actualChainHandlers
     * @param interceptedMethod
     * @param invocationContext
     * @return
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws SecurityException
     * @throws IllegalStateException , if no handler was responsible for the
     * given input.
     */
    private Optional<?> processChainHandlers(Set<Bean<?>> actualChainHandlers, Method interceptedMethod, InvocationContext invocationContext) throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, IllegalAccessException, SecurityException {
        for (Bean<?> actualChainHandler : actualChainHandlers) {
            Optional<?> optionalResult = findAndInvokeBeanInstance(actualChainHandler, interceptedMethod, invocationContext);
            if (optionalResult.isPresent()) {
                return optionalResult;
            }
        }
        throw new IllegalStateException("No ChainHandler was responsible for the intercepted Chain-Controller Method " + invocationContext.getMethod().getName());
    }

    private Optional<?> findAndInvokeBeanInstance(Bean<?> actualChainHandler, Method interceptedMethod, InvocationContext invocationContext) throws NoSuchMethodException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException {
        Object beanInstance = annotationUtil.findBeanInstanceInScopeOfBean((Bean<Object>) actualChainHandler);
        Method beanMethod = actualChainHandler.getBeanClass().getMethod(interceptedMethod.getName(), interceptedMethod.getParameterTypes());
        Optional<?> optionalResult = (Optional<?>) beanMethod.invoke(beanInstance, invocationContext.getParameters());
        return optionalResult;
    }

    private Set<Bean<?>> detectChainHandlers(Method interceptedMethod) {
        Class<? extends Annotation> customQualifierTypeOfChainController = annotationUtil.assertAndGetFirstQualifierTypeOfMethod(interceptedMethod);
        final Set<Bean<?>> potentialChainHandlers = beanManager.getBeans(Object.class, new ChainOfResponsibilityHandlerAnnotationLiteral());
        final Set<Bean<?>> actualChainHandlers = annotationUtil.filterBeansWithAnnotatedMethod(potentialChainHandlers, customQualifierTypeOfChainController);
        return actualChainHandlers;
    }

    private void assertMethodReturnsOptional(Method method) {
        if (!Optional.class.isAssignableFrom(method.getReturnType())) {
            throw new IllegalStateException("Intercepted Chain-Controller Method has the wrong return type. Must be some java.util.Optional!");
        }
    }

}
