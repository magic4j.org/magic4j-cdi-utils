/*
 * Copyright 2016 Stephan Bauer <sb at stephanbauer.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.magic4j.cdiutils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.spi.Context;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Qualifier;

/**
 *
 * @author Stephan Bauer
 */
@ApplicationScoped
public class AnnotationUtil {

    @Inject
    private BeanManager beanManager;

    /**
     * the given method must have a custom CDI-Qualifier Annotation, i.e. an
     * Annotation that is itself annotated with {@link Qualifier}.
     *
     * @param method a method that is annotated with a custom CDI-Qualifier and
     * intercepted by one of the magic4j-interceptors.
     * @return The Class of that Annotation, that is itself annotated as a
     * {@link Qualifier}.
     * @throws IllegalStateException , if no Qualifier Annotation is present
     * because then the application is misconfigured.
     */
    public Class<? extends Annotation> assertAndGetFirstQualifierTypeOfMethod(Method method) {
        Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
        for (Annotation declaredAnnotation : declaredAnnotations) {
            Class<? extends Annotation> annotationType = declaredAnnotation.annotationType();
            if (annotationType.isAnnotationPresent(Qualifier.class)) {
                return annotationType;
            }
        }
        throw new IllegalStateException("First Parameter of Observer-Method has no Qualifier-Annotation");
    }

    public Object findBeanInstanceInScopeOfBean(Bean<Object> bean) {
        //Object reference = beanManager.getReference(bean, null, beanManager.createCreationalContext(bean));
        Context context = beanManager.getContext(bean.getScope());
        Object beanInstance = context.get(bean);
        if (beanInstance == null) {
            CreationalContext<Object> creationalContext = beanManager.createCreationalContext(bean);
            beanInstance = context.get(bean, creationalContext);
        }
        return beanInstance;
    }

    public Set<Bean<?>> filterBeansWithAnnotatedMethod(Set<Bean<?>> potentialChainHandlers, Class<? extends Annotation> customQualifierTypeOfChainController) {
        return potentialChainHandlers.stream().filter(bean -> beanHasMethodWithRequiredQualifierType(bean, customQualifierTypeOfChainController)).collect(Collectors.toSet());
    }

    private boolean beanHasMethodWithRequiredQualifierType(Bean<?> bean, Class<? extends Annotation> customQualifierTypeOfChainController) {
        for (Method m : bean.getBeanClass().getDeclaredMethods()) {
            if (m.getDeclaredAnnotation(customQualifierTypeOfChainController) != null) {
                return true;
            }
        }
        return false;
    }

}
