/*
 * Copyright 2016 Stephan Bauer <sb at stephanbauer.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.magic4j.cdiutils.eventdrivendesign;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.spi.Context;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Qualifier;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * Interceptor that injects the return value of an Observer-Method of a
 * CDI-Event into a properly annotated instance variable, usually of the
 * Event-Producer itself. Therefore, the Observer-Method must be annotated with
 * the corresponding Interceptor-Binding annotation {@link EventResultProvider}.
 * Observer-Methods annotated with {@link EventResultProvider} must have a
 * return type because otherwise it doesn't make sense to intercept that method
 * with this Interceptor.
 * <p>
 * The Bean that declares the injection point for the return value needs to be
 * annotated with the {@link EventResultReceiver} and the instance variable
 * itself must be annotated with the same custom CDI-Qualifier as is the
 * Observer-Method.
 * <p>
 * In the following example, the Observer-Method is annotated with the custom
 * RetrieveOrder annotation.
 * <pre>
 * <code>
 *  &#64;EventResultProvider
 * public String retrieveOrderById(&#64;Observes &#64;RetrieveOrder Long orderId) {
 * ...
 * }
 * </code>
 * </pre>
 * <p>
 * In the following snippet, the injection point for the return value in the
 * Event-Producer is also annotated with the RetrieveOrder annotation:
 * <pre>
 * <code>
 * &#64;EventResultReceiver
 * &#64;RequestScoped
 * &#64;Path("v1/orders") public class OrdersResource {
 *
 * &#64;Inject
 * &#64;RetrieveOrder private Event&lt;Long&gt; retrieveOrderByIdEvent;
 *
 * &#64;RetrieveOrder private String retrievedOrderAsXml;
 *
 * }
 * </code>
 * </pre>
 *
 * A Bean that declares an injection point for the return value must be
 * RequestScoped and annotated with {@link EventResultReceiver} (see above).
 *
 * @author Stephan Bauer
 */
@EventResultProvider
@Interceptor
public class EventResultProviderInterceptor {

    @Inject
    BeanManager beanManager;

    @AroundInvoke
    public Object provideEventResponseToCaller(InvocationContext invocationContext) throws Exception {
        Object result = invocationContext.proceed();
        Class<? extends Annotation> qualifierAnnotationTypeOfObservedEvent = assertAndGetQualifierAnnotationOfObserverMethod(invocationContext.getMethod());
        Optional<Bean<?>> optionalBeanWithQualifiedResultVariable = findBeanWithQualifiedResultVariable(qualifierAnnotationTypeOfObservedEvent, result.getClass());
        if (optionalBeanWithQualifiedResultVariable.isPresent()) {
            findAndUpdateBeanInstance(optionalBeanWithQualifiedResultVariable.get(), qualifierAnnotationTypeOfObservedEvent, result);
        }
        return result;
    }

    private void findAndUpdateBeanInstance(Bean<?> beanWithQualifiedResultVariable, Class<? extends Annotation> qualifierAnnotationTypeOfObservedEvent, Object result) throws IllegalStateException, SecurityException {
        Object beanInstance = findBeanInstanceInRequestScope(beanWithQualifiedResultVariable);
        if (beanInstance != null) {
            Field responseField = findQualifiedFieldInBean(beanWithQualifiedResultVariable, qualifierAnnotationTypeOfObservedEvent, result.getClass());
            tryUpdateAttributeValueOfBeanInstance(responseField, beanInstance, result);
        }
    }

    private Class<? extends Annotation> assertAndGetQualifierAnnotationOfObserverMethod(Method method) {
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        assertObservesAnnotationIsPresent(parameterAnnotations[0]);
        for (Annotation parameterAnnotation : parameterAnnotations[0]) {
            Class<? extends Annotation> annotationType = parameterAnnotation.annotationType();
            if (annotationType.isAnnotationPresent(Qualifier.class)) {
                return annotationType;
            }
        }
        throw new IllegalStateException("First Parameter of Observer-Method has no Qualifier-Annotation");
    }

    private void assertObservesAnnotationIsPresent(Annotation[] parameterAnnotation) {
        for (Annotation annotation : parameterAnnotation) {
            if (annotation instanceof Observes) {
                return;
            }
        }
        throw new IllegalStateException("Observes-Annotation not found on intercepted method");
    }

    private Optional<Bean<?>> findBeanWithQualifiedResultVariable(Class<? extends Annotation> qualifierAnnotationTypeOfObservedEvent, Class<?> resultClass) {
        Set<Bean<?>> beans = beanManager.getBeans(Object.class, new EventResultReceiverAnnotationLiteral());
        for (Bean<?> bean : beans) {
            Field[] declaredFields = bean.getBeanClass().getDeclaredFields();
            for (int i = 0; i < declaredFields.length; i++) {
                Field declaredField = declaredFields[i];
                Annotation declaredAnnotation = declaredField.getDeclaredAnnotation(qualifierAnnotationTypeOfObservedEvent);
                if (declaredAnnotation != null && declaredField.getType().equals(resultClass)) {
                    return Optional.of(bean);
                }
            }
        }
        return Optional.empty();
    }

    private Field findQualifiedFieldInBean(Bean<?> bean, Class<? extends Annotation> qualifierAnnotationTypeOfObservedEvent, Class<?> resultClass) {
        Field[] declaredFields = bean.getBeanClass().getDeclaredFields();
        for (int i = 0; i < declaredFields.length; i++) {
            Field declaredField = declaredFields[i];
            boolean annotationPresent = declaredField.isAnnotationPresent(qualifierAnnotationTypeOfObservedEvent);
            if (annotationPresent && declaredField.getType().equals(resultClass)) {
                return declaredField;
            }
        }
        throw new IllegalStateException("Bean " + bean.getBeanClass().getSimpleName() + " hat kein Field mit der ewrwarteten Annotation " + qualifierAnnotationTypeOfObservedEvent.getSimpleName());
    }

    private Object findBeanInstanceInRequestScope(Bean<?> eventProducerBean) {
        Context requestScopedContext = beanManager.getContext(RequestScoped.class);
        Object beanInstance = requestScopedContext.get(eventProducerBean);
        return beanInstance;
    }

    private void tryUpdateAttributeValueOfBeanInstance(Field declaredField, Object beanInstance, Object result) throws SecurityException, IllegalStateException {
        declaredField.setAccessible(true);
        try {
            declaredField.set(beanInstance, result);
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            throw new IllegalStateException(ex.toString(), ex);
        }
    }

}
