/*
 * Copyright 2016 Stephan Bauer <sb at stephanbauer.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.magic4j.cdiutils.chainofresponsibility;

import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Stephan Bauer <sb at stephanbauer.me>
 */
@ApplicationScoped
public class SomeBusinessChainController implements SomeBusiness {

    @ChainOfResponsibilityController
    @SomeSpecificUsecaseQualifier
    @Override
    public Optional<String> doSomething() {
        throw new IllegalStateException("Should not have been called.");
    }

}
