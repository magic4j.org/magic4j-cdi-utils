/*
 * Copyright 2016 Stephan Bauer <sb at stephanbauer.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.magic4j.cdiutils.chainofresponsibility;

import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Stephan Bauer <sb at stephanbauer.me>
 */
@ApplicationScoped
@ChainOfResponsibilityHandler
public class DoSomethingChainHandlerA implements SomeBusiness {

    /**
     * this hanlder shall not be responsible for the request.
     *
     * @return returns an empty Optional as it is not responsible.
     */
    @Override
    @SomeSpecificUsecaseQualifier
    public Optional<String> doSomething() {
        return Optional.empty();
    }

}
