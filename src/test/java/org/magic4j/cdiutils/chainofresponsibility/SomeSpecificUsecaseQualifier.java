/*
 *Copyright (C) 2015 by Stephan Bauer, www.stephanbauer.me
 *This file is under the "Must-Make-The-Code-Your-Own" License
 *see http://www.stephanbauer.me/mmtcyo-license-terms.html
 *
 *In short, you must make the code your own by copying the content into your own java sources or pom files.
 *This means that you are not allowed to create dependencies on this original file or the module in which
 *it is included. Also you are not allowed to reuse the root package name "me.stephanbauer". You must copy
 *the code into your own classes of your own packages and your own modules. Also you are not allowed to
 *redistribute the package or parts of it.
 */
package org.magic4j.cdiutils.chainofresponsibility;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.inject.Qualifier;

/**
 * @author stephan
 */
@Qualifier
@Retention(RUNTIME)
@Target({
    METHOD, FIELD, PARAMETER, TYPE
})
public @interface SomeSpecificUsecaseQualifier {

}
